/**
 * @aut David Lorenzo (david.lorenzo.nunez@gmail.com)
 * @since 2014-07-07
 * 
 * This class models a percolation system of a NxN matrix. Each cell of the
 * grid can 
 */

//import java.lang.IllegalArgumentException;
//import java.lang.IndexOutOfBoundsException;
    
public class Percolation {
    private boolean[][] grid; // False -> cell is blocked. True -> cell is open
    private int N; // Size of the grid
    private WeightedQuickUnionUF top; // grid connected to the top
    private WeightedQuickUnionUF bottom; // grid connected to the bottom
    private int topId; // top virtual element
    private int bottomId; // bottom virtual element
    private boolean percolated;
    

   /**
    * Class constructor. Create a N-by-N grid, with all sites blocked
    * <p>
    * @param N Size of the grid to be created. It has to be a natural value 
    * greater than 0
    * 
    */
    public Percolation(int N) {
        if (N <= 0) {
            throw new java.lang.IllegalArgumentException("The percolation grid has to have a size greater than 0 and received a size of " + Integer.toString(N));
        }
        this.grid = new boolean[N][N];
        this.N = N;
        
        // grid initialization to false, ie all cells are blocked
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                this.grid[i][j] = false;
            }
        }
        
        this.percolated = false;
        
        // top and bottom virtual element id
        this.topId = 0;
        this.bottomId = this.N*this.N + 1;
        
        // +2 for the virtual elements
        this.top    = new WeightedQuickUnionUF(N*N+2);
        this.bottom = new WeightedQuickUnionUF(N*N+2);
    }
    
    /**
     * Open a site (row i, column j) of the grid if it is not already open
     * <p>
     * @param i row number (1 <= i <= N)
     * @param j column number (1<= j <= N)
     */
    public void open(int i, int j) {
        int li = txIndex(i);
        int lj = txIndex(j);
        if (isOpen(i, j)) return;
        this.grid[li][lj] = true;

        //if site at the top, union with virtual element 0
        if (i == 1) this.top.union(txtSiteToUF(i, j), this.topId);

        //if site at the bottom, union with 'bottom' virtual element
        if (i == this.N) this.bottom.union(txtSiteToUF(i, j), this.bottomId);
        
        //only make the union if both sites are open
        // top union find
        if (i > 1 && isOpen(i-1, j)) this.top.union(txtSiteToUF(i, j), txtSiteToUF(i-1, j)); // site above
        if (i < N && isOpen(i+1, j)) this.top.union(txtSiteToUF(i, j), txtSiteToUF(i+1, j)); // site below
        if (j > 1 && isOpen(i, j-1)) this.top.union(txtSiteToUF(i, j), txtSiteToUF(i, j-1)); // site at the left
        if (j < N && isOpen(i, j+1)) this.top.union(txtSiteToUF(i, j), txtSiteToUF(i, j+1)); // site at the right

        // bottom union find
        if (i > 1 && isOpen(i-1, j)) this.bottom.union(txtSiteToUF(i, j), txtSiteToUF(i-1, j)); // site above
        if (i < N && isOpen(i+1, j)) this.bottom.union(txtSiteToUF(i, j), txtSiteToUF(i+1, j)); // site below
        if (j > 1 && isOpen(i, j-1)) this.bottom.union(txtSiteToUF(i, j), txtSiteToUF(i, j-1)); // site at the left
        if (j < N && isOpen(i, j+1)) this.bottom.union(txtSiteToUF(i, j), txtSiteToUF(i, j+1)); // site at the right

        if (!this.percolated) {
            if (this.top.connected(this.topId, txtSiteToUF(i, j)) &&
                this.bottom.connected(this.bottomId, txtSiteToUF(i, j))) {
                this.percolated = true;
            }
        }
    }
    
    /**
     * Returns whether the site at row i, column j is open or not
     * <p>
     * Checks if i, j are in the valid range and then returns the state of the site.
     * Throws an exception if i or j are not valid.
     * <p>
     * @param i row number (1 <= i <= N)
     * @param j column number (1 <= i <= N)
     * @return true if the site is open, false if it is not open
     */
    public boolean isOpen(int i, int j) {
        int li = txIndex(i);
        int lj = txIndex(j);
        return this.grid[li][lj];
    }
    
    
    /**
     * is site (row i, column j) full?
     * <p>
     * @param i row number. 1 <= i <= N
     * @param j column number. 1 <= j <= N
     * @return true if the site is full, false if the site is empty
     */
    public boolean isFull(int i, int j) {
        int lid = txtSiteToUF(i, j);
        return this.top.connected(this.topId, lid);
    }
    
    /**
     * does the system percolate?
     * <p>
     * if any of the sites in the last row is full then the system has percolated
     * <p>
     * @return true if the system percolates, false if the system doesn't percolate
     */
    public boolean percolates() {
        return this.percolated;
    }
    
    /**
     * Translates an index from the API format (1 <= idx <= N) to a correct value
     * for the internal data structure (0 <= idx* <= N-1)
     * <p>
     * This method checks that the index is a valid row or column 
     * in the range 1 <= idx <= N
     * <p>
     * @param idx the index we are validating. It could be a row or a column
     * @return the value of the index translated to the internal data structure
     */
    private int txIndex(int idx) {
        if (idx <= 0 || this.N < idx)
            throw new java.lang.IndexOutOfBoundsException("Invalid site");
        return idx-1;
    }
    
    /**
     * Translates a site location (row i, row j) to a Union Find identifier
     * <p>
     * id = 0 is reserved
     * <p>
     * @param i row number 1 <= i <= N
     * @param j column number 1 <= i <= N
     * @return the id for the union find data structure. id = (i-1)*N + j
     */
    private int txtSiteToUF(int i, int j) {
        int li = txIndex(i);
        int lj = txIndex(j);
        return li*this.N + lj + 1;
    }

    public static void main(String [] args) {
        int N = 20;
        if (args.length > 0)
            N = Integer.parseInt(args[0]);
        int col = N/2;
        Percolation test = new Percolation(N);
        for (int i = 1; i <= N/2; i++) {
            test.open(i, col);
            StdOut.println("open row i = " + Integer.toString(i) + " percolates = "
                               + Boolean.toString(test.percolates()));
        }
        for (int i = N; i >= N/2; i--) {
            test.open(i, col+1);
            StdOut.println("open row i = " + Integer.toString(i) + " percolates = "
                               + Boolean.toString(test.percolates()));
        }
    }
}
