/**
 * @aut David Lorenzo (david.lorenzo.nunez@gmail.com)
 * @since 2014-07-08
 * 
 * This class will perform several simulations of a percolation system and 
 * calculate a value of the percolation treshold.
 * 
 */

public class PercolationStats {
    private int N; // size of the percolation system to simulate
    private int T; // number of percolation executions to perform
    private double [] simResults; // all the percolation simulations results

    /**
     * Constructor of the class creates an array to store the results
     * for T independent simulations on a percolation system of size NxN
     * <p>
     * @param N size of the percolation systems (grids of NxN) to simulate
     * @param T number of independent simulations to perform
     * @throw java.lang.IllegalArgumentException if N or T is <= 0
     */
    public PercolationStats(int N, int T) {
        this.N = N;
        this.T = T;
        
        if (this.N <= 0) throw new java.lang.IllegalArgumentException("The size N given is not valid. Must be greater than 0");
        if (this.T <= 0) throw new java.lang.IllegalArgumentException("The number of simulations T given is not valid. Must be greater than 0");
        
        this.simResults = new double[T];

        // T independent simulations
        for (int t = 0; t < T; t++) {
            int acu = 0;
            Percolation sim = new Percolation(N);
            while (!sim.percolates()) {
                int i = StdRandom.uniform(1, N + 1);
                int j = StdRandom.uniform(1, N + 1);
                if (!sim.isOpen(i, j)) {
                    acu++;
                    sim.open(i, j);
                }
            }
            simResults[t] = ((double) acu) / (N * N);
        }
    }
    
    /**
     * Calculates the sample mean of percolation threshold
     * <p>
     * @return mean value
     */
    public double mean() {
        return StdStats.mean(this.simResults);
    }
    /**
     * Calculates the sample standard deviation of percolation threshold
     * <p>
     * @return standard deviation value
     */
    public double stddev() {
        return StdStats.stddev(this.simResults);
    }

    /**
     * returns lower bound of the 95% confidence interval
     */
    public double confidenceLo() {
        return mean() - 1.96*stddev()/Math.sqrt(this.T);
    }
    
    /**
     * returns upper bound of the 95% confidence interval
     */
    public double confidenceHi() { 
        return mean() + 1.96*stddev()/Math.sqrt(this.T);
    }

    /*
    private int countOpenSites(Percolation o) {
        int acu = 0;
        for (int i = 1; i <= this.N; i++) {
            for (int j = 1; j <= this.N; j++) {
                if (o.isOpen(i, j)) {
                    acu++;
                }
            }
        }
        return acu;
    }
    */

    public static void main(String[] args) {
        if (args.length != 2) {
            StdOut.println("Not enough parameters");
            StdOut.println("Usage:");
            StdOut.println("progname N T");
            StdOut.println("   N: Size of the percolation systems to simulate. A grid of NxN cells");
            StdOut.println("   T: Number of simulations to perform");
            return;
        }
        
        int N = Integer.parseInt(args[0]);
        int T = Integer.parseInt(args[1]);
        PercolationStats percStats = new PercolationStats(N, T);
        
        StdOut.println("mean                    = " + Double.toString(percStats.mean()));
        StdOut.println("stddev                  = " + Double.toString(percStats.stddev()));
        StdOut.println("95% confidence interval = " + Double.toString(percStats.confidenceLo()) + ", " + Double.toString(percStats.confidenceHi()));
    }
}