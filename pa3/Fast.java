import java.util.Arrays;

public class Fast {
    public static void main(String [] args) {
        String filename = args[0];
        In in = new In(filename);
        int N = in.readInt();
        Point [] points = new Point[N];
        
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);
        StdDraw.setPenRadius(0.01);  // make the points a bit larger

        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }
        
        Arrays.sort(points);
        StdDraw.setPenRadius();
        for (int i = 0; i < points.length; i++) {
            Point p = points[i];
            //Point [] sl = copyArray(points, i);
            Point [] sl = (Point []) Arrays.copyOf(points, points.length);
            Arrays.sort(sl, p.SLOPE_ORDER);
            //debugPrint(p, sl);
            printSegments(p, sl);
        }
        
        StdDraw.show(0);
    }
    
    /*
    private static Point [] copyArray(Point [] points, int k) {
        Point [] copy = new Point[points.length];
        for (int i = 0; i < points.length; i++) {
            copy[i] = points[i];
        }
        return copy;
    }
    //*/
    
    private static void debugPrint(Point p, Point [] points) {
        StdOut.println("---------- " + p);
        for (int i = 0; i < points.length; i++) {
            StdOut.print(points[i] + " [" + p.slopeTo(points[i]) + "] ");
        }
        StdOut.println("\n");
    }
    
    private static void printSegments(Point p, Point [] points) {
        int lo = 0;
        double slpp = p.slopeTo(points[0]);
        int i;
        for (i = 1; i < points.length; i++) {
            double slpi = p.slopeTo(points[i]);
            if (slpp != slpi) {
                checkSegment(p, points, lo, i);
                slpp = slpi;
                lo = i;
            }
        }
        checkSegment(p, points, lo, i);
    }
    
    private static void checkSegment(Point p, Point [] array, int lo, int hi) {
        if (hi - lo < 3) return;
        for(int i = lo; i < hi; i++) {
            if (p.compareTo(array[i]) > 0) {
                return;
            }
        }
        
        // p is the smallest point in the subarray
        printSegment(p, array, lo, hi);
        p.drawTo(array[hi-1]);
        return;
    }

    private static void printSegment(Point p, Point [] points, int lo, int hi) {
        String segment = p.toString();
        for (int i = lo; i < hi; i++) {
            segment += " -> " + points[i];
        }
        StdOut.println(segment);
    }
}
