import java.util.Arrays;

public class Brute {
    public static void main(String [] args) {
        // read in the input
        String filename = args[0];
        In in = new In(filename);
        int N = in.readInt();
        Point [] points = new Point[N];
        
        // rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);
        StdDraw.setPenRadius(0.01);  // make the points a bit larger
        
        for (int i = 0; i < N; i++) {
            int x = in.readInt();
            int y = in.readInt();
            points[i] = new Point(x, y);
            points[i].draw();
        }
        
        /*
        for (Point p : points) {
            StdOut.println(p);
        }
        */
        StdDraw.setPenRadius();  // make the points a bit larger
        Arrays.sort(points);
        for (int i = 0; i < points.length; i++) {
            Point p1 = points[i];
            for (int j = i + 1; j < points.length; j++) {
                Point p2 = points[j];
                double s2 = p1.slopeTo(p2);
                for (int k = j + 1; k < points.length; k++) {
                    Point p3 = points[k];
                    double s3 = p1.slopeTo(p3);
                    if (s3 != s2) continue;
                    for (int l = k + 1; l < points.length; l++) {
                        Point p4 = points[l];
                        double s4 = p1.slopeTo(p4);
                        if (s4 != s3) continue;
                        StdOut.println(p1 + " -> " + p2 + " -> " + p3 + " -> " + p4);
                        p1.drawTo(p4);
                    }
                }
            }
        }
        // display to screen all at once
        StdDraw.show(0);
        
        //usar una cola para comparar si la ruta ya fue considerada
    }
}
