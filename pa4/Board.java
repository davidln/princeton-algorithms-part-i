public class Board {
    private byte[][] blocks;
    private int N = 0;
    private int manhattan = -1;
    //private Board parent = null;
    
    public Board(int[][] blocks) {          // construct a board from an N-by-N array of blocks
                                            // (where blocks[i][j] = block in row i, column j)
        N = blocks.length;
        this.blocks = new byte[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                this.blocks[i][j] = (byte) blocks[i][j];
            }
        }
    }
                                           
    public int dimension() {                // board dimension N
        return this.N;
    }
    
    public int hamming() {                  // number of blocks out of place
        int hamming = 0;
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                //StdOut.print(" " + this.blocks[i][j] + "|" + i + "," + j + "|" + (this.N*i + j + 1));
                if (this.blocks[i][j] == 0) continue;
                if (this.blocks[i][j] != this.N*i + j + 1) {
                    //StdOut.print("*");
                    hamming++;
                }
                //StdOut.print("\n");
            }
        }
        return hamming;
    }
    
    public int manhattan() {                // sum of Manhattan distances between blocks and goal
        if (this.manhattan >= 0) return this.manhattan;
        this.manhattan = 0;
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                if (this.blocks[i][j] == 0) continue;
                int gi = this.blocks[i][j] / this.N;
                int gj = this.blocks[i][j] % this.N;
                if (gj == 0) {
                    gi--;
                    gj = this.N -1;
                } else {
                    gj--;
                }
                //StdOut.println(" " + this.blocks[i][j] + " : " + i + "," + j + "|" + gi + "," + gj + " " + (Math.abs(gi - i) + Math.abs(gj - j)));
                this.manhattan += Math.abs(gi - i) + Math.abs(gj - j);
            }
        }
        return this.manhattan;
    }

    public boolean isGoal() {               // is this board the goal board?
        for (int i = 0; i < this.N - 1; i++)
            for (int j = 0; j < this.N; j++)
                if (this.blocks[i][j] != i*this.N + j + 1) return false;
        
        for (int j = 0; j < this.N - 1; j++)
            if (this.blocks[this.N - 1][j] != (this.N - 1)*this.N + j + 1) return false;

        if (0 != this.blocks[this.N - 1][this.N - 1]) return false;
        return true;
    }
    
    public Board twin() {                   // a board obtained by exchanging two adjacent blocks in the same row
        Board twin = new Board(copy());
        if (twin.blocks[0][0] == 0 || twin.blocks[0][1] == 0) {
            byte aux           = twin.blocks[1][1];
            twin.blocks[1][1] = twin.blocks[1][0];
            twin.blocks[1][0] = aux;

        } else {
            byte aux           = twin.blocks[0][1];
            twin.blocks[0][1] = twin.blocks[0][0];
            twin.blocks[0][0] = aux;
        }
        return twin;
    }

    public boolean equals(Object y) {       // does this board equal y?
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board ny = (Board) y;
        if (this.dimension() != ny.dimension()) return false;
        for (int i = 0; i < this.N; i++)
            for (int j = 0; j < this.N; j++)
                if (this.blocks[i][j] != ny.blocks[i][j]) return false;
        return true;
    }
    
    private int [][] copy() {
        int [][] c = new int[this.N][this.N];
        for (int i = 0; i < this.N; i++)
            for (int j = 0; j < this.N; j++)
                c[i][j] = this.blocks[i][j];
        return c;
    }
    
    private int [][] swapZeroLeft(int i, int j) {
        int [][] ret = copy();
        if (j == 0) return null;
        ret[i][j]   = ret[i][j-1];
        ret[i][j-1] = 0;
        return ret;
    }

    private int [][] swapZeroRight(int i, int j) {
        int [][] ret = copy();
        if (j == this.N - 1) return null;
        ret[i][j]   = ret[i][j+1];
        ret[i][j+1] = 0;
        return ret;
    }

    private int [][] swapZeroAbove(int i, int j) {
        int [][] ret = copy();
        if (i == 0) return null;
        ret[i][j]   = ret[i-1][j];
        ret[i-1][j] = 0;
        return ret;
    }

    private int [][] swapZeroBelow(int i, int j) {
        int [][] ret = copy();
        if (i == this.N - 1) return null;
        ret[i][j]   = ret[i+1][j];
        ret[i+1][j] = 0;
        return ret;
    }

    public Iterable<Board> neighbors() {    // all neighboring boards
        int zi = 0, zj = 0;
        for (int i = 0; i < this.N; i++)
            for (int j = 0; j < this.N; j++)
                if (this.blocks[i][j] == 0) {
                    zi = i;
                    zj = j;
                }
        
        int [][] left  = swapZeroLeft(zi, zj);
        int [][] right = swapZeroRight(zi, zj);
        int [][] above = swapZeroAbove(zi, zj);
        int [][] below = swapZeroBelow(zi, zj);

        Stack<Board> alternatives = new Stack<Board>();
        if (left  != null) alternatives.push(new Board(left));
        if (right != null) alternatives.push(new Board(right));
        if (above != null) alternatives.push(new Board(above));
        if (below != null) alternatives.push(new Board(below));        
        return alternatives;
    }

    /*
    public String toString() {              // string representation of the board (in the output format specified below)
        String strBoard = Integer.toString(this.N) + "\n";
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                strBoard += " " + blocks[i][j];
            }
            strBoard += "\n";
        }
        return strBoard;
    }
    */
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        for (int i = 0; i < this.N; i++) {
            for (int j = 0; j < this.N; j++) {
                s.append(String.format("%2d ", this.blocks[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    }

    public static void main(String[] args) {
        int [][] test1 = new int[3][3];
        
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                test1[i][j] = 3*i + j + 1;
        test1[2][2] = 0;
        
        Board t1 = new Board(test1);

        StdOut.println("---------------");
        StdOut.print(t1.toString());
        StdOut.println("Test is goal?: " + Boolean.toString(t1.isGoal()));
        StdOut.println("---------------");
        
        Board testEqual = new Board(test1);
        StdOut.println("---------------");
        StdOut.print(t1.toString());
        StdOut.print(testEqual.toString());
        StdOut.println("Test equals: " + Boolean.toString(t1.equals(testEqual)));
        StdOut.println("---------------");

        int [][] b4 = new int[4][4];
        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                b4[i][j] = 3*i + j + 1;
        Board testD4 = new Board(b4);
        StdOut.println("---------------");
        StdOut.print(t1.toString());
        StdOut.print(testD4.toString());
        StdOut.println("Test equals: " + Boolean.toString(t1.equals(testD4)));
        StdOut.println("---------------");

        int aux = test1[0][0];
        test1[0][0] = test1[0][1];
        test1[0][1] = aux;
        Board t2 = new Board(test1);
        StdOut.println("---------------");
        StdOut.print(t2.toString());
        StdOut.println("Test is goal?: " + Boolean.toString(t2.isGoal()));
        StdOut.println("---------------");
        
        StdOut.println("---------------");
        StdOut.print(t1.toString());
        StdOut.print(t2.toString());
        StdOut.println("Test is goal?: " + Boolean.toString(t1.equals(t2)));
        StdOut.println("---------------");

        StdOut.println("---------------");
        StdOut.print(t1.toString());
        Board orig = new Board(test1);
        Board twin = orig.twin();
        StdOut.print(twin.toString());
        StdOut.println("---------------");

        Board corner = new Board(test1);
        StdOut.println("---------------");
        StdOut.println(corner.toString());
        StdOut.println("Trying neighbors:");
        for (Board b : corner.neighbors()) {
            StdOut.println(b.toString());
        }
        StdOut.println("Check:");
        StdOut.println(corner.toString());
        StdOut.println("---------------");

        Board z0 = null;
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {0, 1, 2}, {3, 4, 5}, {6, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");

        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 0, 2}, {3, 4, 5}, {6, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 0}, {3, 4, 5}, {6, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 3}, {0, 4, 5}, {6, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 3}, {4, 0, 5}, {6, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 3}, {4, 5, 0}, {6, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 3}, {4, 5, 6}, {0, 7, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 3}, {4, 5, 6}, {7, 0, 8} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");
        StdOut.println("+++++++++++++++++");
        z0 = new Board(new int [][]{ {1, 2, 3}, {4, 5, 6}, {7, 8, 0} });
        StdOut.println(z0.toString());
        for (Board b : z0.neighbors())
            StdOut.println(b.toString());
        StdOut.println("+++++++++++++++++");

        Board hamming = new Board(new int [][]{ {1, 2, 3}, {4, 0, 5}, {6, 7, 8} });
        StdOut.println("-----------------");
        StdOut.println(hamming.toString());
        StdOut.println("Hamming: " + hamming.hamming());

        Board hamming2 = new Board(new int [][]{ {8, 1, 3}, {4, 0, 2}, {7, 6, 5} });
        StdOut.println("-----------------");
        StdOut.println(hamming2.toString());
        StdOut.println("Hamming: " + hamming2.hamming());
        StdOut.println("Manhattan: " + hamming2.manhattan());
        StdOut.println("Manhattan: " + hamming2.manhattan());
    }
}