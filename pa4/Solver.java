public class Solver {
    private boolean solvable = true;
    private Node solution = null;
    private Node twinSolution = null;

    public Solver(Board initial) {           // find a solution to the initial board (using the A* algorithm)
        Node initialGame = new Node();
        initialGame.parent = null;
        initialGame.moves  = 0;
        initialGame.board  = initial;
        
        Node twin = new Node();
        twin.parent = null;
        twin.moves  = 0;
        twin.board  = initial.twin();

        MinPQ<Solver.Node> initialPQ = new MinPQ<Solver.Node>();
        MinPQ<Solver.Node> twinPQ    = new MinPQ<Solver.Node>();
        
        //ST<Board, Node> initialST = new ST<Board, Node>();
        //ST<Board, Node> twinST    = new ST<Board, Node>();
        
        initialPQ.insert(initialGame);
        //initialST.insert(initialGame.board, initialGame);
        
        twinPQ.insert(twin);
        //twinST.insert(twin.board, twin);

        do {
            if (solution == null) {
                // once we get a solution it is useless keep on working on the twin
                Node twinNextMin    = twinPQ.delMin();
                if (twinNextMin.board.isGoal()) {
                    this.solvable = false;
                    twinSolution = twinNextMin;
                    break;
                }
                
                for (Board b : twinNextMin.board.neighbors()) {
                    if (twinNextMin.parent == null || !b.equals(twinNextMin.parent.board)) {
                        Node newTwin   = new Node();
                        newTwin.board  = b;
                        newTwin.parent = twinNextMin;
                        newTwin.moves  = twinNextMin.moves + 1;
                        twinPQ.insert(newTwin);
                    }
                }
            }
            
            if (initialPQ.isEmpty()) break;
            Node initialNextMin = initialPQ.delMin();
            if (initialNextMin.board.isGoal()) {
                if (solution == null || solution.moves > initialNextMin.moves) {
                    solution = initialNextMin;
                    continue;
                }
            }
            for (Board b : initialNextMin.board.neighbors()) {
                if (initialNextMin.parent == null || !b.equals(initialNextMin.parent.board)) {
                    Node newInitial   = new Node();
                    newInitial.board  = b;
                    newInitial.parent = initialNextMin;
                    newInitial.moves  = initialNextMin.moves + 1;
                    initialPQ.insert(newInitial);
                }
            }
            
        } while (solvable && solution == null);
    }

    public boolean isSolvable() {            // is the initial board solvable?
        return solvable;
    }

    public int moves() {                     // min number of moves to solve initial board; -1 if no solution
        if (solution == null) return -1;
        return solution.moves;
    }

    public Iterable<Board> solution() {      // sequence of boards in a shortest solution; null if no solution
        if (!solvable) return null;
        Stack<Board> steps = new Stack<Board>();
        Node goal = solution;
        while (goal != null) {
            steps.push(goal.board);
            goal = goal.parent;
        }
        return steps;
    }
    
    private Iterable<Board> twinSolution() {
        Stack<Board> steps = new Stack<Board>();
        Node goal = twinSolution;
        while (goal != null) {
            steps.push(goal.board);
            goal = goal.parent;
        }
        return steps;
    }
    
    private class Node implements Comparable<Node> {
        private Board board;
        private Node parent;
        private int moves;
        
        public int compareTo(Node that) {
            int scoreA = this.moves + this.board.manhattan();
            int scoreB = that.moves + that.board.manhattan();
            
            if (scoreA == scoreB) return this.moves - that.moves;
            return scoreA - scoreB;
        }
    }

    public static void main(String[] args) { // solve a slider puzzle (given below)
        //Board test = new Board( new int[][]{ {0, 1, 3}, {4, 2, 5}, {7, 8, 6} });
        Board test = new Board(new int[][]{ {0, 1, 3}, {4, 2, 5}, {7, 8, 6} });
        Solver puzzle = new Solver(test);
        
        if (puzzle.isSolvable()) {
            for (Board b : puzzle.solution()) {
                StdOut.println(b.toString());
            }
        } else {
            StdOut.println("Puzzle is not solvable");
            for (Board b : puzzle.twinSolution()) {
                StdOut.println(b.toString());
            }
        }
    }
}