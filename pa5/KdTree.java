public class KdTree {
    private int N = 0;
    private Node root = null;

    public KdTree() {                               // construct an empty set of points
    }
    public boolean isEmpty() {                       // is the set empty?
        return N == 0;
    }

    public int size() {                              // number of points in the set
        return N;
    }

    public void insert(Point2D p) {                  // add the point p to the set (if it is not already in the set)
        if (p == null) return;
        if (root == null) {
            root = new Node(p, new RectHV(0.0, 0.0, 1.0, 1.0), null, null, Node.VERTICAL);
            N++;
        } else {
            insert(root, p);
        }
    }

    private void insert(Node n, Point2D p) {
        if (n.p.equals(p)) return;
        if (n.type == Node.HORIZONTAL) {
            if (p.y() < n.p.y()) {
                // going left or insert left
                if (n.lb == null) {
                    n.lb = new Node(p, new RectHV(n.rect.xmin(), n.rect.ymin(), n.rect.xmax(), n.p.y())
                                           , null, null, Node.VERTICAL);
                    N++;
                } else {
                    insert(n.lb, p);
                }
            } else {
                // going right or insert right
                if (n.rt == null) {
                    n.rt = new Node(p,  new RectHV(n.rect.xmin(), n.p.y(), n.rect.xmax(), n.rect.ymax())
                                           , null, null, Node.VERTICAL);
                    N++;
                } else {
                    insert(n.rt, p);
                }
            }
        } else {
            if (p.x() < n.p.x()) {
                // going bottom or insert bottom
                if (n.lb == null) {
                    n.lb = new Node(p,  new RectHV(n.rect.xmin(), n.rect.ymin(), n.p.x(), n.rect.ymax())
                                           , null, null, Node.HORIZONTAL);
                    N++;
                } else {
                    insert(n.lb, p);
                }
            } else {
                // going top or insert top
                if (n.rt == null) {
                    n.rt = new Node(p,  new RectHV(n.p.x(), n.rect.ymin(), n.rect.xmax(), n.rect.ymax())
                                           , null, null, Node.HORIZONTAL);
                    N++;
                } else {
                    insert(n.rt, p);
                }
            }
        }
    }

    public boolean contains(Point2D p) {             // does the set contain the point p?
        if (p == null || root == null) return false;
        return contains(root, p);
    }
    
    private boolean contains(Node n, Point2D p) {
        if (n == null)                 return false;
        if (n.p == p || n.p.equals(p)) return true;

        if (n.type == Node.HORIZONTAL) {
            if (p.y() < n.p.y()) {
                return contains(n.lb, p);
            } else {
                return contains(n.rt, p);
            }
        } else {
            if (p.x() < n.p.x()) {
                return contains(n.lb, p);
            } else {
                return contains(n.rt, p);
            }
        }
    }
    
    public void draw() {                             // draw all of the points to standard draw
        draw(root);
    }

    private void draw(Node n) {
        // traversing the tree in order
        if (n == null) return;
        draw(n.lb);
        
        StdDraw.setPenRadius(.01);
        StdDraw.setPenColor(StdDraw.BLACK);
        n.p.draw();
        StdDraw.setPenRadius();
        if (n.type == Node.HORIZONTAL) {
            StdDraw.setPenColor(StdDraw.BLUE);
            StdDraw.line(n.rect.xmin(), n.p.y(), n.rect.xmax(), n.p.y());
        } else {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.line(n.p.x(), n.rect.ymin(), n.p.x(), n.rect.ymax());
        }
        
        draw(n.rt);
    }

    public Iterable<Point2D> range(RectHV rect) {    // all points in the set that are inside the rectangle
        Stack<Point2D> pointsInRect = new Stack<Point2D>();
        range(root, rect, pointsInRect);
        return pointsInRect;
    }
    
    private void range(Node n, RectHV rect, Stack<Point2D> pointsInRect) {
        if (n == null) return;
        if (rect.contains(n.p)) pointsInRect.push(n.p);

        if (n.type == Node.HORIZONTAL) {
            if (rect.ymin() <= n.p.y()) range(n.lb, rect, pointsInRect);
            if (rect.ymax() >= n.p.y()) range(n.rt, rect, pointsInRect);
        } else {
            if (rect.xmin() <= n.p.x()) range(n.lb, rect, pointsInRect);
            if (rect.xmax() >= n.p.x()) range(n.rt, rect, pointsInRect);
        }
    }
    
    public Point2D nearest(Point2D p) {              // a nearest neighbor in the set to p; null if set is empty
        if (p == null || root == null) return null;
        return nearest(root, p, root.p);
    }
    
    private Point2D nearest(Node n, Point2D p, Point2D closestPoint) {
        if (n == null) return closestPoint;

        Point2D closest = new Point2D(closestPoint.x(), closestPoint.y());
        if (n.p.distanceSquaredTo(p) < closest.distanceSquaredTo(p))
            closest = n.p;

        if (n.type == Node.VERTICAL) {
            if (p.x() < n.p.x()) {
                closest = nearest(n.lb, p, closest);
                if (n.rt != null && n.rt.rect.distanceSquaredTo(p) < closest.distanceSquaredTo(p))
                    closest = nearest(n.rt, p, closest);
            } else {
                closest = nearest(n.rt, p, closest);
                if (n.lb != null && n.lb.rect.distanceSquaredTo(p) < closest.distanceSquaredTo(p))
                    closest = nearest(n.lb, p, closest);
            }
        } else {
            if (p.y() < n.p.y()) {
                closest = nearest(n.lb, p, closest);
                if (n.rt != null && n.rt.rect.distanceSquaredTo(p) < closest.distanceSquaredTo(p))
                    closest = nearest(n.rt, p, closest);
            } else {
                closest = nearest(n.rt, p, closest);
                if (n.lb != null && n.lb.rect.distanceSquaredTo(p) < closest.distanceSquaredTo(p))
                    closest = nearest(n.lb, p, closest);
            }
        }
        return closest;
    }
    
    private static class Node {
        private Point2D p;      // the point
        private RectHV rect;    // the axis-aligned rectangle corresponding to this node
        private Node lb;        // the left/bottom subtree
        private Node rt;        // the right/top subtree
        private boolean type;   // vertical or horizontal node
        public static final boolean HORIZONTAL = true;
        public static final boolean VERTICAL   = false;
        public Node(Point2D p, RectHV rect, Node lb, Node rt, boolean type) {
            this.p    = p;
            this.rect = rect;
            this.lb   = lb;
            this.rt   = rt;
            this.type = type;
        }
    }
    
    public static void main(String[] args) {
        String filename = args[0];
        In in = new In(filename);

        StdDraw.show(0);

        // initialize the two data structures with point from standard input
        KdTree kdtree = new KdTree();
        while (!in.isEmpty()) {
            double x = in.readDouble();
            double y = in.readDouble();
            Point2D p = new Point2D(x, y);
            kdtree.insert(p);
        }
        
        kdtree.draw();
        StdDraw.show(0);
    }
}