public class PointSET {
    private SET<Point2D> points = null;
    public PointSET() {                              // construct an empty set of points
        points = new SET<Point2D>();
    }
    public boolean isEmpty() {                       // is the set empty?
        return points.isEmpty();
    }
    public int size() {                              // number of points in the set
        return points.size();
    }
    public void insert(Point2D p) {                  // add the point p to the set (if it is not already in the set)
        if (p != null && points != null)
            points.add(p);
    }
    public boolean contains(Point2D p) {             // does the set contain the point p?
        if (p == null || points == null)
            return false;
        return points.contains(p);
    }
    
    public void draw() {                             // draw all of the points to standard draw
        StdDraw.show(0);
        StdDraw.setPenRadius(0.01);
        for (Point2D p : points) {
            p.draw();
        }
        StdDraw.show(0);        
    }

    public Iterable<Point2D> range(RectHV rect) {    // all points in the set that are inside the rectangle
        if (rect == null || points == null || points.isEmpty()) return null;
        Stack<Point2D> stackRange = new Stack<Point2D>();
        for (Point2D p : points) {
            if (rect.contains(p))
                stackRange.push(p);
        }
        return stackRange;
    }
    
    public Point2D nearest(Point2D p) {              // a nearest neighbor in the set to p; null if set is empty
        if (p == null || points == null || points.isEmpty()) return null;
        Point2D nearestPoint = null;
        double distance = Double.POSITIVE_INFINITY;
        
        for (Point2D q : points) {
            double nextDistance = p.distanceSquaredTo(q);
            if (nextDistance < distance) {
                nearestPoint = q;
                distance = nextDistance;
            }
        }
        return nearestPoint;
    }
}