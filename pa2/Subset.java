/**
 * @aut David Lorenzo (david.lorenzo.nunez@gmail.com)
 * @since 2014-07-11
 * 
 * Randomized queue client test. Receives several strings from stdin
 * and a parameter with the size of a subset and returns a random
 * subset
 */

public class Subset {

    public static void main(String [] args) {
        RandomizedQueue<String> rq = new RandomizedQueue<String>();
        String [] input = StdIn.readAllStrings();
        int k = Integer.parseInt(args[0]);
        
        for (int i = 0; i < input.length; i++) {
            rq.enqueue(input[i]);
        }
        
        for (int i = 0; i < k; i++) {
            StdOut.println(rq.dequeue());
        }
    }
}