/**
 * @aut David Lorenzo (david.lorenzo.nunez@gmail.com)
 * @since 2014-07-10
 * 
 * Randomized queue data structure implementation with a resizing array
 */
import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
        
    private Item [] array;
    private int N = 0;
    
    public RandomizedQueue() {                // construct an empty randomized queue
        array = (Item []) new Object[1];
    }

    /**
     * True if the queue has no items, False otherwise
     * */
    public boolean isEmpty() {                // is the queue empty?
        return (N <= 0);
    }
    
    /**
     * Returns the number of items in the queue
     * */
    public int size() {
        return N;
    }
    
    /**
     * Add an item to the queue
     * */
    public void enqueue(Item item) {
        if (item == null) { throw new NullPointerException("Cannot add a null item to the Randomized queue"); }
        if (N == array.length) { resize(2 * array.length); }
        array[N++] = item;
    }
 
    /**
     * delete and return a random item
     * */
    public Item dequeue()  {
        if (isEmpty()) { throw new NoSuchElementException("The queue is empty"); }
        int i = selectRandom();
        Item item = array[i];
        array[i] = array[--N];
        array[N] = null;
        if (N > 0 && N < array.length/4) { resize(array.length / 2); }
        return item;
    }
    
    /**
     * return (but do not delete) a random item
     * */
    public Item sample() {
        if (isEmpty()) { throw new NoSuchElementException("The queue is empty"); }
        return array[selectRandom()];
    }
    
    private int selectRandom() {
        return StdRandom.uniform(N);
    }
    
    private void resize(int capacity) {
        Item [] newArray = (Item []) new Object[capacity];
        for (int i = 0; i < N; i++) {
            newArray[i] = array[i];
        }
        
        array = newArray;
    }
    
    public Iterator<Item> iterator() {        // return an independent iterator over items in random order
       return new RandomizedQueueIterator();
    }
    

    private class RandomizedQueueIterator implements Iterator<Item> {
        private int [] order;
        private int current = 0;
        public RandomizedQueueIterator() {
            order = new int[N];
            for (int i = 0; i < N; i++) {
                order[i] = i;
            }
            StdRandom.setSeed(System.nanoTime());
            StdRandom.shuffle(order);
        }
        public boolean hasNext() { return (current < order.length); }
        public void remove() { throw new UnsupportedOperationException("Remove from the Deque is not supported"); }
        public Item next() {
            if (current == N) { throw new java.util.NoSuchElementException("There is no 'next' item"); }
            return array[order[current++]];
        }
    }

    public static void main(String[] args) {  // unit testing
         RandomizedQueue<Integer> rqi = new RandomizedQueue<Integer>();
         for (int i = 0; i < 20; i++) {
             rqi.enqueue(i);
         }

         Iterator<Integer> ii1 = rqi.iterator();
         Iterator<Integer> ii2 = rqi.iterator();
         while (ii1.hasNext()) {
             StdOut.println("Value1 : " + Integer.toString(ii1.next()));
             while (ii2.hasNext())
                 StdOut.println("Value2 : " + Integer.toString(ii2.next()));
         }
    }
}