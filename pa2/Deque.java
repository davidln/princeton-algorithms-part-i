/**
 * @aut David Lorenzo (david.lorenzo.nunez@gmail.com)
 * @since 2014-07-10
 * 
 * Double ended queue data structure implementation with linked lists
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private class Node {
        private Item item;
        private Node next;
        private Node prev;
    }
    
    private Node head = null;
    private Node tail = null;
    private int size = 0;
   
    public Deque() {                           // construct an empty deque
        
    }

    /**
     * This method returns if the deque has no items
     * @return True if empty, False if there are any items in the data structure
     */
    public boolean isEmpty() {                // is the deque empty?
        return size == 0;
    }
    /**
     * This method gives the number of elements stored in the deque
     * @return the number of items
     * */
    public int size() {                       // return the number of items on the deque
        return size;
    }
    
    /**
     * Add an item at the front of the deque
     * */
    public void addFirst(Item item) {         // insert the item at the front
        if (item == null) { throw new NullPointerException("No object passed"); }
        Node node = new Node();
        node.item = item;
        
        if (isEmpty()) {
            head = node;
            tail = node;
            node.prev = null;
            node.next = null;
        } else {
            node.prev = null;
            node.next = head;
            head.prev = node;
            head      = node;
        }
        size++;
    }

    /**
     * Add an item at the end of the deque
     * */
    public void addLast(Item item) {          // insert the item at the end
        if (item == null) { throw new NullPointerException("No object passed"); }
        Node node = new Node();
        node.item = item;
        
        if (isEmpty()) {
            head = node;
            tail = node;
            node.prev = null;
            node.next = null;
        } else {
            node.next = null;
            node.prev = tail;
            tail.next = node;
            tail      = node;
        }
        size++;
    }

    /**
     * Remove the first element in the deque and return that item to the caller
     * */
    public Item removeFirst() {               // delete and return the item at the front
        if (isEmpty()) { throw new NoSuchElementException("The Deque is empty"); }
        
        Item item = head.item;
        
        if (size == 1) {
            head = null;
            tail = null;
        } else {
            Node second = head.next;
            second.prev = null;
            head        = second;
        }
        
        size--;
        return item;
    }
    /**
     * Remove the last element in the deque and return that item to the caller
     * */
    public Item removeLast() {                // delete and return the item at the end
        if (isEmpty()) { throw new NoSuchElementException("The Deque is empty"); }
        
        Item item = tail.item;
        
        if (size == 1) {
            head = null;
            tail = null;
        }
        else {
            Node beforeLast = tail.prev;
            beforeLast.next = null;
            tail             = beforeLast;
        }
        
        size--;
        return item;
    }
    
    /**
     * Returns an iterator of the deque starting at the first element in the front
     * and moving forward one item at a time until it reaches the last element
     * */
    public Iterator<Item> iterator() {        // return an iterator over items in order from front to end
        return new DequeIterator();
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current = head;
        public boolean hasNext() { return (current != null); }
        public void remove() { throw new UnsupportedOperationException("Remove from the Deque is not supported"); }
        public Item next() {
            if (current == null) { throw new java.util.NoSuchElementException("There is no 'next' item"); }
            Item item = current.item;
            current = current.next;
            return item;
        }
    }
    
    public static void main(String[] args) {  // unit testing
        Deque<Integer> di = new Deque<Integer>();
        
        di.addFirst(10);
//*
        di.addFirst(5);
        di.addFirst(2);
        di.addLast(15);
        
        
        StdOut.println("---------------");
        for (int i : di) StdOut.println(i);
//*/
        di.removeLast();
        StdOut.println("---------------");
        for (int i : di) StdOut.println(i);
        
        di.removeFirst();
        StdOut.println("---------------");
        for (int i : di) StdOut.println(i);
        
        di.removeFirst();
        StdOut.println("---------------");
        for (int i : di) StdOut.println(i);
        
        di.removeLast();
        StdOut.println("---------------");
        for (int i : di) StdOut.println(i);
    }
}